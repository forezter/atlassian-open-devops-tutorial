# Intro


## Prerequisites

### Jira Cloud 
- You have created a Jira Software account, and a Jira Software project.
- You're familiar with the basics of Jira issues.

[Get it free!](https://www.atlassian.com/software/jira/free)

### Bitbucket Cloud 
- You have created a Bitbucket cloud account.
- You're familiar with the basics of Bitbucket.

[Get it free!](https://www.atlassian.com/software/bitbucket/bundle)

### JFrog Cloud 
- You have access to your own JFrog Cloud instance.
- You're familiar with the basics of the JFrog.

[Get it free!](https://jfrog.com/start-free/#saas)

### JFrog Platform: Artifactory App for Jira 
- You have installed the _JFrog Platform: Artifactory App for Jira_.
- You have followed the instructions to configure your JFrog Cloud instances with the app and tested the connectivity.

[Get it free!](https://marketplace.atlassian.com/apps/1220479/jfrog-platform-artifactory-app-for-jira?hosting=cloud&tab=overview)



# Step 1: Get the Example Code



1. Go to [https://bitbucket.org/jefffjfrog/atlassian-open-devops-tutorial](https://bitbucket.org/jefffjfrog/atlassian-open-devops-tutorial).
2. Fork this repository to your Bitbucket workspace.


# Step 2: Set up JFrog Pipelines for Jira



1. Next, follow these [instructions](https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/) to create a Jira API Token.
2. Copy this Jira API Token.
3. In your JFrog Platform instance, set up the Jira integration following these [instructions](https://www.jfrog.com/confluence/display/JFROG/Managing+Pipelines+Integrations#ManagingPipelinesIntegrations-AddinganIntegration).
4. Choose **Jira** for the **Integration Type**.
5. Call the Integration _jiraForAtlassianTutorial._
6. Specify the Jira server API endpoint as the **url**.
7. Specify your Jira **User Name**.
8. Paste your Jira API Token for **Token**.
9. Click **Create** to create the Jira integration.

# Step 3: Set up JFrog Pipelines for Artifactory

1. Now create an Artifactory integration with the same instructions from **Step 3**.
2. Choose **Artifactory** for the **Integration Type**.
3. Call the Integration _artifactoryForAtlassianTutorial._
4. Use the provided **Artifactory URL** and **User**.
5. Generate and API key using the **Get API Key** button.
6. Click **Create** to create the Artifactory integration.

# Step 4: Set up JFrog Pipelines for Bitbucket

1. Next, follow these [instructions](https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/) to create a Bitbucket App password.
2. Copy this Bitbucket App password.
3. Now create a Bitbucket integration with the same instructions from **Step 4**.
4. Choose **Bitbucket** for the **Integration Type**.
5. Call the Integration _bitbucketForAtlassianTutorial._
6. Specify your Bitbucket **User Name**.
7. Paste your Bitbucket App password for **Token**.
8. Click **Create** to create the Bitbucket integration.

# Step 5: Build the Example Code

1. In your JFrog Platform instance, add the example code following these [instructions](https://www.jfrog.com/confluence/display/JFROG/Managing+Pipeline+Sources#ManagingPipelineSources-AddingaPipelineSource) using **From YAML**.
2. Specify _bitbucketForAtlassianTutorial_ for the SCM Provider Integration.
3. Specify _your username/atlassian-open-devops-tutorial_ for the repository name.
4. Use the default values for the remaining fields.
5. Click **Create Source**.
6. Wait a few seconds and JFrog Pipelines will attempt to build the example application in your _atlassian-open-devops-tutorial _repository. It will fail due to an issue with the Dockerfile.
7. Go to your Jira project and view the new issue titled _Atlassian tutorial failed build_.

# Step 6: Fix the Dockerfile

1. Go to your Bitbucket forked repository _atlassian-open-devops-tutorial _in your browser.
2. **Edit** the **Dockerfile**.
3. Delete the line that says _“This line is not recognized and will cause the build to fail!”_.
4. Commit this change and include the Jira issue number in the commit message.
5. Wait a few seconds and JFrog Pipelines will attempt to build the example application in your _atlassian-open-devops-tutorial _repository again. But this time it will succeed.

# Step 7: View the Fix in Jira and JFrog Artifactory

1. Now go back to your Jira issue.
2. Notice that the ticket now references a build.
3. Click on the build.
4. This will bring up a dialog that shows the JFrog pipeline and the build number. You can click on this to be taken directly to the build in JFrog Cloud.
5. Click on the Issues tab in the build view in JFrog Artifactory.
6. Notice that this issue is now referenced with this build as well.

